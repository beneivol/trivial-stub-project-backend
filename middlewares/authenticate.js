'use strict';

var jwt = require('jwt-simple');
var moment = require('moment');

// TODO definir esta clave de forma más segura
const secret = 'claveSecreta';

function ensureAuth(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(403).send({message: 'La petición no tiene la cabecera de autorización'});
    }

    var token = req.headers.authorization;
    console.log('Token en authenticate - ensureAuth ' + token);

    try {
        var payload = jwt.decode(token, secret);

        if (payload.exp <= moment().unix()){
            return res.status(401).send({message: 'El token ha expirado'});
        }
    } catch(ex) {
        return res.status(404).send({message: 'Token no válido'});
    }

    req.user = payload;


    // Llamamos a next para salir del middleware
    next();
}

module.exports = {
    ensureAuth: ensureAuth
};