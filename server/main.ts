const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const user_routes = require('../routes/user.routes');

// Utilizamos promesas globales, sin esto aparece mensaje de promesas mongoose deprecadas
mongoose.Promise = global.Promise;

// Mejoramos la respuesta de salida en las peticiones
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Agregamos la palabra 'api' para todas las rutas
app.use('/api', user_routes);

// Conexión a la base de datos
mongoose.connect('mongodb://localhost:27017/trivialDev', { useMongoClient: true }).then(
    (db) => { console.log('Conectado a la base de datos -> ' + db.name) },
    (error) => { console.log('* * * Error al conectar a la base de datos -> ' + error) }
);









// Acciones en el socket
io.on('connection', function (socket) {
    let totalClientsConnected = io.engine.clientsCount;
    io.emit('get-total-clients', {
        socketId: socket.id,
        totalClientsConnected: totalClientsConnected
    });

    let currentDate = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    console.log(currentDate  + ' - Socket connected to Server');
    console.log('Socket Server: on connection - ' + currentDate);


    // Events

    // Event add-message
    socket.on('add-message', function(user, message) {
        console.log('Socket Server: on add-message');

        console.log('User ->' + user);
        console.log('Message ->' + message);

        io.emit('new-message', {
            user: user,
            text: message
        });

        // Function above that stores the message in the database
        // databaseStore(message)
    });



    // Event disconnect
    socket.on('disconnect', function() {
        console.log('Socket Server: on disconnect');
        totalClientsConnected = io.engine.clientsCount;
        io.emit('get-total-clients', {
            socketId: socket.id,
            totalClientsConnected: totalClientsConnected
        });
    });

});

// Lanza el servidor en el puerto 8080
server.listen(8080, function () {
    console.log('Server started on http://localhost:8080');
});