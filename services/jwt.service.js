'use strict';

var jwt = require('jwt-simple');
var moment = require('moment');

// TODO definir esta clave de forma más segura
const secret = 'claveSecreta';

function createToken(user) {
    var payload = {
        username: user.username,
        email: user.email,
        iat: moment().unix(),
        exp: moment().add(1, 'day').unix()
    };

    return jwt.encode(payload, secret);
}

module.exports = {
    createToken: createToken
};
