'use strict';

var bcrypt = require('bcrypt-nodejs');
var jwtService = require('../services/jwt.service');
var User = require('../models/user.model');

function pruebas(req, res) {
    res.status(200).send({message: 'probando-controlador'});
}

function loginUser (req, res) {
    var params = req.body;

    var email = params.email;
    var password = params.password;

    User.findOne({email: email.toLowerCase()}, function (err, user) {
        if (!err && user) {
            // Comprobamos la contraseña
            bcrypt.compare(password, user.password, function (err, check) {
                if(check) {
                    // Creamos el token y lo devolvemos
                    var token = jwtService.createToken(user);
                    res.status(200).send({username: user.username, token: token});
                } else {
                    res.status(400).send({message: 'Usuario/Contraseña incorrectos'});
                }
            });
        } else {
            res.status(500).send({message: 'Error en el login'});
        }
    })
}

function saveUser(req, res) {
    var user = new User();

    // Parámetros que vienen por POST en el body
    var params = req.body;

    user.username = params.username;
    user.email = params.email;
    user.password = params.password;
    user.role = 'role_user'; // Por ahora hardcodeado

    if(params.password) {
        //Encriptamos la contraseña
        bcrypt.hash(params.password, null, null, function(err, hash) {
            if (!err) {
                user.password = hash;

                // Guardamos el usuario en la base de datos
                user.save(function(err, userStored) {
                   if (!err && userStored) {
                       console.log('Usuario almacenado: ' + userStored);
                       res.status(200).send({user: userStored});
                       // res.status(200).send({message: 'Usuario guardado con éxito'});
                   } else {
                       res.status(500).send({message: 'Error al guardar usuario en base de datos'});
                   }
                });


            } else {
                res.status(400).send({message: 'Error al cifrar contraseña'});
            }
        });


    } else {
        res.status(400).send({message: 'Contraseña vacía'});
    }
}

module.exports = {
    pruebas: pruebas,
    loginUser: loginUser,
    saveUser: saveUser
};