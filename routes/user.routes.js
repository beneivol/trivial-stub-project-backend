'use strict';

var express = require('express');
var api = express.Router();

var md_auth = require('../middlewares/authenticate');

var UserController = require('../controllers/user.controller');

// Añadimos el middleware como segundo parámetro de esta ruta concreta
api.get('/probando-controlador', md_auth.ensureAuth, UserController.pruebas);

api.post('/login', UserController.loginUser);
api.post('/register', UserController.saveUser);

module.exports = api;
